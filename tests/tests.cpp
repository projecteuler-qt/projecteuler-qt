#include <QtTest/QtTest>
#include "functions.h"

EXTERN QString Problem010();

class tests : public QObject
{
	Q_OBJECT
private slots:
	void p10(QString a = "142913828922") {
		QBENCHMARK {
			Problem010();
		}
		QVERIFY(Problem010() == a);
	}

};

QTEST_MAIN(tests)
#include "tests.moc"
