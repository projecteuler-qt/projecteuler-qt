TEMPLATE = subdirs
SUBDIRS = sub_problems \
	sub_gui \
#	sub_test

sub_problems.subdir = Problems

sub_gui.subdir = gui
sub_gui.depends = sub_problems

sub_test.file = tests/tests.pro
sub_test.depends = sub_problems

# See http://www.qtcentre.org/wiki/index.php?title=Undocumented_qmake for the fancy subtarget notation
