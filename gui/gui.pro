include (../pe-gui.pri)
TARGET = projectEuler-qt
TEMPLATE = app
CONFIG += silent
DESTDIR = ..

SOURCES += main.cpp \
    mainwindow.cpp \
    problemstate.cpp \
    compiler.cpp
HEADERS += \
    mainwindow.h \
    problemstate.h \
    compiler.h
FORMS += mainwindow.ui
RESOURCES += data.qrc

macx {
	CONFIG -= app_bundle
}

