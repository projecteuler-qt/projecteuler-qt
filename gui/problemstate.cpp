#include <iostream>

#include <QFutureWatcher>
#include <QDebug>
#include <QTime>

#include "problemstate.h"

#include <qtconcurrentrun.h>
using namespace QtConcurrent;

ProblemState::ProblemState(QString (*new_p)(), QString name) :
		QFutureWatcher<QString>(0)
{
	name.remove(QRegExp("Problem"));
	m_num=QVariant(name).toInt();
	twi_num.setText(name);

	QTableWidget *tw = global_ui_pointer->tableWidget;
	tw->setRowCount(tw->rowCount() + 1);
	tw->setItem(m_num-1, 0, &twi_num);
	tw->setItem(m_num-1, 1, &twi_ans);
	tw->setItem(m_num-1, 2, &twi_ms);

	connect(this, SIGNAL(finished()), this, SLOT(stopTimer()));
	setP(new_p);
}

void ProblemState::setP(QString (*new_p)())
{
	m_p = new_p;
	if (m_p) {
		twi_ans.setText(tr("Calculating..."));
	} else {
		m_p = dummyproblem;
		setAns(tr("N/A"));
	}

	QMetaObject::invokeMethod(this, "go", Qt::QueuedConnection);
}

void ProblemState::go()
{
	if (m_p != dummyproblem) {
		setMs(QTime());
		setFuture(run(*m_p));
	} else {

	}
}

ProblemState::~ProblemState()
{
}

int ProblemState::getNum()
{
	return m_num;
}

QString ProblemState::getAns()
{
	return m_ans;
}

void ProblemState::setAns(QString ans)
{
	m_ans = ans;
	twi_ans.setText(ans);
	global_ui_pointer->tableWidget->resizeColumnsToContents();
}

QTime ProblemState::getMs()
{
	return m_timer;
}

void ProblemState::setMs(QTime t)
{
	if (!t.isValid()) {
		m_timer = t;
		m_timer.start();
		twi_ms.setText(tr("---"));
	}
}

void ProblemState::stopTimer() {

	if (m_p != dummyproblem) {
		m_runtime = m_timer.elapsed();
		twi_ms.setText(QVariant(m_runtime).toString());
		setAns(result());
	}

//	qDebug()	<< m_num
//			<< qPrintable(result())
//			<< m_timer.elapsed();
}
