#include <QApplication>
#include <QDebug>
#include <QTimer>

#include "mainwindow.h"
#include "compiler.h"

#ifndef QT_NO_CONCURRENT
#include <qtconcurrentrun.h>
using namespace QtConcurrent;

// Hack
Ui::MainWindow *global_ui_pointer;

/**
 * shorthand identifies are only good in a local context otherwise meaning is lost
 * later on in the code, don't confuse yourself
 */
int main(int argc, char *argv[])
{
	QApplication app(argc,argv);

	MainWindow *d = new MainWindow();
	d->show();

//	QTimer::singleShot(0, d->C, SLOT(linkProblemsIn()));
	QMetaObject::invokeMethod(d->C, "linkProblemsIn", Qt::QueuedConnection);

	return app.exec();
}

#else //QT_NO_CONCURRENT
int main()
{
	qDebug() << "Qt Concurrent is not yet supported on this platform";
}
#endif
