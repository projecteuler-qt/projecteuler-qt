#include <iostream>

#include <QDebug>
#include <QMainWindow>
#include <QFile>
#include <QMessageBox>
#include <QString>
#include <QTextStream>
#include <QTimer>

#include "compiler.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "problemstate.h"

MainWindow::MainWindow(QWidget *parent) :
		QMainWindow(parent),
		ui(new Ui::MainWindow)
{
	C = new Compiler;

	ui->setupUi(this);
	global_ui_pointer = ui;	// from problemstate.h

	connect(ui->compileButton, SIGNAL(clicked()), C, SLOT(recompile()), Qt::QueuedConnection);
	connect(ui->goButton, SIGNAL(clicked()), C, SLOT(linkProblemsIn()), Qt::QueuedConnection);
	connect(ui->saveButton, SIGNAL(clicked()), this, SLOT(save()), Qt::QueuedConnection);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
	QMainWindow::changeEvent(e);
	switch (e->type()) {
	case QEvent::LanguageChange:
		ui->retranslateUi(this);
		break;
	default:
		break;
	}
}

void MainWindow::selectProblem(int x, int y)
{
	Q_UNUSED(y);

	//open new file
	QString problemName = TOPSRCDIR;
	problemName.append("/Problems/Problem");
	int s = problemName.size();
	problemName.append(QString("%1").arg(x+1, 3, 10, QChar('0')));
	problemName.truncate(s+3);
	problemName.append(".cpp");
	QFile file(problemName);

	if (!file.open(QIODevice::ReadOnly)) {
		ui->editor->clear();
		currentFileName = "";
	} else {
		QTextStream stream(&file);
		ui->editor->setPlainText(stream.readAll());
		file.close();
		currentFileName = problemName;
	}
	setWindowTitle(tr("Project Euler: %1").arg(currentFileName));
}

void MainWindow::save()
{
	QFile file(currentFileName.toLatin1());

	if (!file.open((QIODevice::WriteOnly | QIODevice::Truncate))) {
		qDebug() << "Failed to write" << currentFileName;
		QMessageBox nofilewarning;
		nofilewarning.setText(tr("Could not write to %1").arg(file.fileName()));
		nofilewarning.exec();
		return;
	} else {
		QTextStream stream(&file);
		stream << ui->editor->toPlainText();
		file.close();
	}
	//QTimer::singleShot(0, this, SLOT(compile())); // We have a button for this
}
