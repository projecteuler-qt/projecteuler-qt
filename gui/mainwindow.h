#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QTableWidgetItem;

class Compiler;

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow {
	Q_OBJECT
public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();
	
	Compiler *C;


public slots:
	void save();
	void selectProblem(int x, int y /*ignored*/);

protected:
	void changeEvent(QEvent *e);
	QString resolveNumber(int n);

private:
	Ui::MainWindow *ui;
	QString currentFileName;
};

#endif // MAINWINDOW_H
