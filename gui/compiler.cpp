#include <stdlib.h>

#include <QDir>
#include <QLibrary>
#include <QMessageBox>
#include <QString>

#include "compiler.h"
#include "problemstate.h"	// provides pfp

Compiler::Compiler()
{
	//LIBNAME is derived from compile-time
	lib = new QLibrary(LIBNAME);
	problemstates = new QMap<QString, ProblemState*>;
}

void Compiler::recompile()
{
	lib->unload();
	system ("make sub-Problems-qmake_all");
	system ("make sub-Problems-all");
	linkProblemsIn();
}

void Compiler::linkProblemsIn()
{
	if (!lib->load()) {
		QMessageBox nolibwarning;
		nolibwarning.setText(tr("Could not find %1").arg(LIBNAME));
		nolibwarning.exec();
		return;
	}

	QStringList problemNames;
	for (int i=1; i<999; ++i) {
		QString p = QString("Problem%1").arg(i, 3, 10, QChar('0'));
		p.truncate(10);
		problemNames.append(p);
	}

	// maybe we need to mark what's new?
	foreach (QString name, problemNames) {
		// Resolve from library into problemstates
		ProblemFunctionPointer pfp = (ProblemFunctionPointer)
					     lib->resolve(name.toLatin1());

		if (problemstates->isEmpty() || !((*problemstates)[name])) {
			// First time round
			(*problemstates)[name] = new ProblemState(pfp, name);
		} else {
			// All other times
			(*problemstates)[name]->setP(pfp);
		}
	} // foreach
}
