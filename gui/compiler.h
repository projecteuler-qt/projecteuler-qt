#ifndef COMPILER_H
#define COMPILER_H

#include <QObject>
#include <QLibrary>
#include <QMap>

#define LIBNAME "./libprojecteuler.so"

#ifdef Q_WS_WIN
#undef LIBNAME
#define LIBNAME "projecteuler.dll"
#endif
#ifdef Q_WS_MAC
#undef LIBNAME
#define LIBNAME "libprojecteuler.dylib"
#endif

class QString;
class ProblemState;

class Compiler : public QObject
{
	Q_OBJECT
public:
	Compiler();

public slots:
	void recompile();
	void linkProblemsIn();

private:
	QMap<QString, ProblemState*> *problemstates;
	void relink(QStringList names);
	QLibrary *lib;
};

#endif // COMPILER_H
