#ifndef PROBLEMSTATE_H
#define PROBLEMSTATE_H

#include <QFutureWatcher>
#include <QTime>

#include "ui_mainwindow.h"

class QString;
class QTableWidgetItem;

extern Ui::MainWindow *global_ui_pointer;
typedef QString (*ProblemFunctionPointer)(void);
inline QString dummyproblem() {return QString("N/A");}

class ProblemState : public QFutureWatcher<QString>
{
	Q_OBJECT
	Q_PROPERTY(int num READ getNum)
	Q_PROPERTY(QString ans READ getAns WRITE setAns)
	Q_PROPERTY(QTime ms READ getMs WRITE setMs)

public:
	ProblemState(QString (*new_p)(), QString name);
	void setP(QString (*new_p)());
	~ProblemState();

	int getNum();
	void setNum(int num);
	QString getAns();
	void setAns(QString ans);
	QTime getMs();
	void setMs(QTime t);

private:
	QString (*m_p)();
	QTime m_timer;
	int m_runtime;
	int m_num;			// The projecteuler problem number (index)
	QString m_ans;
	QTableWidgetItem twi_ans;	// display the answer here
	QTableWidgetItem twi_ms;	// disply time elapsed in ms
	QTableWidgetItem twi_num;	// disply problem's number

signals:
	//no signals

public slots:
	void stopTimer();

private slots:
	void go();

};

#endif // PROBLEMSTATE_H
