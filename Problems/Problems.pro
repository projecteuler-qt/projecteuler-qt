TEMPLATE = lib
CONFIG += silent
TARGET = projecteuler

DESTDIR = ..
SOURCES += ./Problem*.cpp
HEADERS += functions.h

macx {
	CONFIG -= app_bundle
}

