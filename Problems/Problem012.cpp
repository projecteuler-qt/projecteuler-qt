#include <cmath>
#include "functions.h"

static int count_divisors(const quint64 num);

EXTERN QString Problem012()
{
	quint64 test_triangle_no = 0;
	quint64 n = 1;
	int divisor_count;

	forever {
		test_triangle_no += n++;
		divisor_count = count_divisors(test_triangle_no);
		if (divisor_count>=500) break;
	}
	return QVariant(test_triangle_no).toString();
}

/**
 * If sqrt(num) is also a divisor, there are an odd number of divisors.
 * All divisors have pairs so we only need to check half of them.
 * ie. up to sqrt(num)
 */
int count_divisors(const quint64 num) {
	int count=0;
	quint64 div;
	quint64 sqt = sqrt(qreal(num));

	if(sqt*sqt == num){
		count--;
	}

	div=sqt;
	do {
		if(!(num%div--)){
			count++;count++;
		}
	} while (div > 0);

	return count;
}
