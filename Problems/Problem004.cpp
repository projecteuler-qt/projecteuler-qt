#include "functions.h"

static quint64 reverse(quint64 n);
static bool isPalindrome(const quint64 n);

EXTERN QString Problem004() {
	int largestPalindrome = 0;
	int a = 999;
	int b;
	int db;
	while(a >= 100) {
		if(a%11 == 0) {
			b = 999;
			db = 1;
		} else {
			b = 990;
			db = 11;
		}
		while(b>=a) {
			if( (a*b) <= largestPalindrome )
				break;
			if(isPalindrome(a*b))
				largestPalindrome = a*b;
			b -= db;
		}
		--a;
	}

	return QVariant(largestPalindrome).toString();
}

static quint64 reverse(quint64 n) {
	quint64 reversed =0;
	while(n>0) {
		reversed = 10*reversed + n%10;
		n=n/10;
	}
	return reversed;
}

static bool isPalindrome(quint64 n) {
	return n == reverse(n);
}
