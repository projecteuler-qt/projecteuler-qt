#include <cmath>
#include <stdlib.h>
#include "functions.h"

EXTERN QString Problem010(void) {
	quint64 const limit = Q_UINT64_C(2000000);
	quint64 search_lim = (quint64) floor(sqrt(qreal(limit)));
	quint64 i,j;
	quint64 sum = 0;
	bool *sieve;

	sieve = (bool*) malloc(sizeof(bool) *  (limit+1));

	for(i=0; i<limit; i++) {
		sieve[i]=true;
	}

	/* zero and 1 are not prime */
	sieve[0] = sieve[1] = false;

	for(i=2; i <= search_lim; i++) {
		if(sieve[i]) {
			/* this is prime, let's bomb the multiples  */
			for(j=i+i;j<limit;j+=i) {
				sieve[j] = false;
			}
		}
	}

	/* find primes and add */
	for(i=1;i<limit;i++) {
		if(sieve[i]) {
			sum+=i;
		}
	}

	free(sieve);
	return QVariant(sum).toString();
}
