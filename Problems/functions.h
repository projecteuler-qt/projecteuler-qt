#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <QtGlobal>
#include <QVariant>
#include <qstring.h>

#ifdef Q_WS_WIN
#define EXTERN extern "C" __declspec(dllexport)
#else
#define EXTERN extern "C"
#endif

#endif // FUNCTIONS_H
