#include <cmath>
#include <iostream>
#include "functions.h"

static bool isPrime(const quint64 n);

EXTERN QString Problem007()
{
	int limit=10001;
	int count=1;
	quint64 candidate = 1;
	for(;count!=limit;) {
		candidate+=2;
		if(isPrime(candidate)) {
			++count;
		}
	}
	return QVariant(candidate).toString();
}

static bool isPrime(const quint64 n)
{
	if (n==1) return false;
	if (n==2) return true;
	if (n==3) return true;
	if (!(n%2)) return false;
	quint64 limit = (quint64) sqrt(qreal(n));
	unsigned int i=3;
	while(i<=limit) {
		if(!(n%i))
			return false;
		i+=2;
	}
	return true;
}
