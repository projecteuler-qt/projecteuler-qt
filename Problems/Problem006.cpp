#include "functions.h"

EXTERN QString Problem006() {
	quint64 sumOfSquares=0, sum=0;
	quint64 n;

	for(n=1;n<=100;++n) {
		sum += n;
		sumOfSquares += n*n;
	}

	sum *= sum;

	return QVariant(sum - sumOfSquares).toString();
}
