#include <cmath>
#include "functions.h"

EXTERN QString Problem005() {
	quint64 k = 20;
	double N = 1.0;
	quint64 i = 0;
	bool check = true;
	quint64 limit = (quint64) sqrt(qreal(k));
	unsigned int p[] = { 2, 3, 5, 7, 11, 13, 17, 19, 21 }; // primes
	unsigned int a[] = { 1, 1, 1, 1,  1,  1,  1,  1,  0};  // exponents

	for(i=0; p[i] < k; ++i) {
		if(p[i] <= limit) {
			a[i] = (int) floor( log10(qreal(k)) / log10(qreal(p[i])) );
		} else {
			check = false;
		}
		N *= pow(qreal(p[i]), int(a[i]));
	}

	return QVariant(N).toString();
}
