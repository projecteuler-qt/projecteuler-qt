#include "functions.h"

static bool ispythagorean_triplet(unsigned int a, unsigned int b, unsigned int c);

EXTERN QString Problem009()
{
	unsigned int a, b, c;
	a=3;
	b=a+1;
	c=1000-a-b;

	while (!ispythagorean_triplet(a,b,c)) {
		while (b<c) {
			if (ispythagorean_triplet(a,b,c))
				goto end;
			++b;
			c=1000-a-b;
		}
		++a;
		b=a+1;
		c=1000-a-b;
	}

end:
	return QVariant(a*b*c).toString();

}

static bool ispythagorean_triplet(unsigned int a, unsigned int b, unsigned int c)
{
	return (a*a + b*b == c*c);
}
