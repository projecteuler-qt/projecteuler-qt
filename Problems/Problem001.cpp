#include "functions.h"

EXTERN QString Problem001() {
	quint64 int_answer = 0;
	for(int i=1; i<1000; ++i)
		if(!(i%3) || !(i%5)) int_answer += i;
	return QVariant(int_answer).toString();
}
