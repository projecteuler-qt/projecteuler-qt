#include <cmath>
#include "functions.h"

EXTERN QString Problem003() {
	quint64 n = Q_UINT64_C(600851475143);
	int factor = 3;
	int lastfactor;
	int maxfactor = sqrt(qreal(n));
	if(n%2 == 0) {
		lastfactor = 2;
		do {
			n /= 2;
		} while(n%2 == 0);
	} else {
		lastfactor = 1;
	}
	while (n>1 && factor<=maxfactor) {
		if (n%factor == 0) {
			lastfactor = factor;
			do {
				n /= factor;
			} while (n%factor == 0);
			maxfactor = sqrt(qreal(n));
		}
		factor +=2;
	}

	if(n ==1) {
		return QVariant(lastfactor).toString();
	} else {
		return QVariant(n).toString();
	}
}
