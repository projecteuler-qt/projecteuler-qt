#include "functions.h"

EXTERN QString Problem002() {
	int a=1, b=2, c;

	quint64 int_answer = 2;
	do {
		c=a+b;
		if (c%2==1)
			int_answer += c;
		a=b;
		b=c;
	} while ((a+b)<4000000);
	return QVariant(int_answer).toString();
}
